class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
      t.string :code
      t.string :title
      t.text :content
      t.text :information
      t.text :assessment
      t.text :material

      t.timestamps
    end
  end
end
