json.extract! course, :id, :code, :title, :content, :information, :assessment, :material, :created_at, :updated_at
json.url course_url(course, format: :json)
